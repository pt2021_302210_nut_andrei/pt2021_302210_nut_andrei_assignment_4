package controllers;

import businessLayer.CompositeProduct;
import businessLayer.DeliveryService;
import presentationLayer.ClientGUI;
import presentationLayer.ProductsManagementGUI;

import static businessLayer.DeliveryService.menuList;
import static businessLayer.DeliveryService.productsList;

public class ProductsManagementController {

    private ProductsManagementGUI productsManagementGUI;
    private DeliveryService deliveryService;

    public ProductsManagementController(DeliveryService deliveryService, ProductsManagementGUI productsManagementGUI){
        this.productsManagementGUI = productsManagementGUI;
        this.deliveryService = deliveryService;
        this.productsManagementGUI.setVisible(true);

        this.productsManagementGUI.addRemoveAction(e ->{
            deliveryService.removeProduct(productsManagementGUI.getRowToDelete());
            productsManagementGUI.deleteRow(productsManagementGUI.getRowToDelete());
        });

        this.productsManagementGUI.addNewProductAction(e->{
            int[] items = productsManagementGUI.getSelectedItems();
            String name = productsManagementGUI.getNewProductName();
            float rating = 0;
            float calories = 0;
            float proteins = 0;
            float fats = 0;
            float sodium = 0;
            float price = 0;
            for(int i : items){
                rating = rating + menuList.get(i).getRating();
                calories = calories + menuList.get(i).getCalories();
                proteins = proteins + menuList.get(i).getProteins();
                fats = fats + menuList.get(i).getFats();
                sodium = sodium + menuList.get(i).getSodium();
                price = price + menuList.get(i).getPrice();
            }
            CompositeProduct ohMy = new CompositeProduct(name,rating,calories,proteins,fats,sodium,price);
            menuList.add(ohMy);
            productsList.add(ohMy);
            productsManagementGUI.addRow(ohMy);
        });

        this.productsManagementGUI.addUpdateAction(e->{
            deliveryService.updateProduct(
                    productsManagementGUI.getRowToDelete(),
                    productsManagementGUI.getTitle(),
                    productsManagementGUI.getRating(),
                    productsManagementGUI.getCalories(),
                    productsManagementGUI.getProteins(),
                    productsManagementGUI.getFats(),
                    productsManagementGUI.getSodium(),
                    productsManagementGUI.getPrice()
            );
            productsManagementGUI.doChanges(productsManagementGUI.getTitle(),
                    productsManagementGUI.getRating(),
                    productsManagementGUI.getCalories(),
                    productsManagementGUI.getProteins(),
                    productsManagementGUI.getFats(),
                    productsManagementGUI.getSodium(),
                    productsManagementGUI.getPrice());

        });
    }

}
