package controllers;

import businessLayer.DeliveryService;
import presentationLayer.ReportGUI;

public class ReportController {

    private ReportGUI reportGUI;
    private DeliveryService deliveryService;

    public ReportController(ReportGUI reportGUI,DeliveryService deliveryService){
        this.reportGUI = reportGUI;
        this.deliveryService = deliveryService;
        this.reportGUI.setVisible(true);

        this.reportGUI.addFirstReport(e ->{
            deliveryService.generateFirstReport(reportGUI.getStartTime(),reportGUI.getFinishTime());
        });

        this.reportGUI.addSecondReport(e->{
            deliveryService.generateSecondReport(reportGUI.getSecondFiled());
        });
        this.reportGUI.addThirdReport(e->{
            deliveryService.generateThirdReport(reportGUI.getOrdersField(),reportGUI.getValue());
        });

        this.reportGUI.addFourthReport(e->{
            deliveryService.generateFourthReport(reportGUI.getDay());
        });
    }
}
