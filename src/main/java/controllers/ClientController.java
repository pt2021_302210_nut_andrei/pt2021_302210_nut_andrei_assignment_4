package controllers;

import businessLayer.*;
import presentationLayer.ClientGUI;
import presentationLayer.EmployeeGUI;

import java.util.ArrayList;
import java.util.List;

import static businessLayer.DeliveryService.menuList;

public class ClientController {

    private ClientGUI clientGUI;
    private int orderNo = 0;
    private DeliveryService deliveryService;

    public ClientController(DeliveryService deliveryService, User client){
        this.deliveryService = deliveryService;
        this.clientGUI = new ClientGUI();
        this.clientGUI.setVisible(true);
        ArrayList<MenuItem> orderedItems = new ArrayList<MenuItem>();
        for(MenuItem x : menuList){
            clientGUI.addRow(x);
        }

        this.clientGUI.addAddOrderListener(e-> {
            orderedItems.add(menuList.get(clientGUI.getSelectedRow()));
        });

        this.clientGUI.addGetOrderBtnListener(e-> {
            float price = 0;
            for(MenuItem a : orderedItems){
                price += a.getPrice();
            }
            deliveryService.createNewOrder(new Order(orderNo++,(Client)client,price),orderedItems);
            orderedItems.clear();
        });

        this.clientGUI.addSearchBtnListener(e-> {
            clientGUI.clearTable();
            List<MenuItem> x;
            x = deliveryService.searchItem(clientGUI.getName(),clientGUI.getRating(),clientGUI.getCalories(),clientGUI.getProteins(),clientGUI.getFats(),clientGUI.getSodium(),clientGUI.getPrice());
            for(MenuItem a : x){
                clientGUI.addRow(a);
            }
        });

    }
}
