package controllers;

import businessLayer.Client;
import businessLayer.DeliveryService;
import businessLayer.Employee;
import presentationLayer.RegisterGUI;
import javax.swing.*;

public class RegisterControl {

    public RegisterControl(RegisterGUI registerGUI){
        registerGUI.setVisible(true);
        registerGUI.addRegisterActionListener(e -> {
            if(DeliveryService.userExists(registerGUI.getUsername())){
                JOptionPane.showMessageDialog(null,"User already exists!");
            }
            else if (registerGUI.getUsername().equals("") || registerGUI.getFirstPassField().equals("")) {
                JOptionPane.showMessageDialog(null, "Please fill all the required information");
            }
            else{
                {
                    if(registerGUI.getFirstPassField().equals(registerGUI.getSecondPassField())){
                        if(registerGUI.getSelectedItem().equals("Customer")){
                            DeliveryService.addUser(new Client(registerGUI.getUsername(),registerGUI.getFirstPassField()));
                        }else{
                            if(registerGUI.getSelectedItem().equals("Employee")){
                                DeliveryService.addUser(new Employee(registerGUI.getUsername(),registerGUI.getFirstPassField()));
                            }
                        }
                        JOptionPane.showMessageDialog(null,"Registration successful!");
                        registerGUI.setVisible(false);
                    }
                    else{
                        JOptionPane.showMessageDialog(null,"Something went wrong!");
                    }
                }
            }
        });
    }

}
