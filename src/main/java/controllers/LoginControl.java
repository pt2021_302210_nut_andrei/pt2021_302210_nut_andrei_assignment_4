package controllers;

import businessLayer.Administrator;
import businessLayer.Client;
import businessLayer.DeliveryService;
import presentationLayer.AdministratorGUI;
import presentationLayer.EmployeeGUI;
import presentationLayer.LoginGUI;
import presentationLayer.RegisterGUI;

import javax.swing.*;

public class LoginControl {

    public LoginControl(LoginGUI loginGUI, DeliveryService deliveryService){
        loginGUI.setVisible(true);
        loginGUI.addLoginActionListener(e -> {
            if(!DeliveryService.userExists(loginGUI.getUsername())){
                JOptionPane.showMessageDialog(null,"User not found!");
            }
            else{
                if(!loginGUI.getPassword().equals(DeliveryService.getUser(loginGUI.getUsername()).getPassword())){
                    JOptionPane.showMessageDialog(null,"Wrong password");
                }
                else{
                    JOptionPane.showMessageDialog(null,"Login successful!");
                    if(DeliveryService.getUser(loginGUI.getUsername()) instanceof Administrator){
                        new AdministratorController(deliveryService);
                    }
                    else if(DeliveryService.getUser(loginGUI.getUsername()) instanceof Client){
                        new ClientController(deliveryService,DeliveryService.getUser(loginGUI.getUsername()));
                    }
                    else{
                        new EmployeeController(new EmployeeGUI());
                    }
                }
            }
        });

        loginGUI.addRegisterActionListener(e -> new RegisterControl(new RegisterGUI()));
    }
}
