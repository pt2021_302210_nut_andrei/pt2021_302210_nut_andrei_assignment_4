package controllers;

import businessLayer.DeliveryService;
import businessLayer.IDeliveryServiceProcessing;
import businessLayer.MenuItem;
import dataLayer.Serializator;
import presentationLayer.AdministratorGUI;
import presentationLayer.ClientGUI;
import presentationLayer.ProductsManagementGUI;
import presentationLayer.ReportGUI;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static businessLayer.DeliveryService.menuList;
import static businessLayer.DeliveryService.productsList;

public class AdministratorController {

    private AdministratorGUI administratorGUI;
    private DeliveryService deliveryService;
    private ProductsManagementGUI productsManagementGUI;

    public AdministratorController(DeliveryService deliveryService){
        this.productsManagementGUI = new ProductsManagementGUI();
        this.deliveryService = deliveryService;
        administratorGUI = new AdministratorGUI();
        administratorGUI.setVisible(true);

        administratorGUI.addImportAction(e -> {
            IDeliveryServiceProcessing.importProducts();
            for(MenuItem x : productsList){
                deliveryService.addMenuItem(x);
                productsManagementGUI.addRow(x);
            }
        });

        administratorGUI.addManageAction(e -> {
            new ProductsManagementController(deliveryService,productsManagementGUI);
        });

        administratorGUI.addGenerateReports(e -> {
            new ReportController(new ReportGUI(),deliveryService);
        });

        administratorGUI.addImportData(e ->
        {
            new Serializator().deserializeData(deliveryService);
            for(MenuItem x : productsList){
                deliveryService.addMenuItem(x);
                productsManagementGUI.addRow(x);
            }
        });
    }
}
