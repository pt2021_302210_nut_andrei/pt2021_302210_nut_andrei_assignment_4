package controllers;

import presentationLayer.EmployeeGUI;

public class EmployeeController {

    private EmployeeGUI employeeGUI;

    public EmployeeController(EmployeeGUI employeeGUI){
        this.employeeGUI = employeeGUI;
        employeeGUI.setVisible(true);

    }
}
