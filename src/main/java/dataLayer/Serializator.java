package dataLayer;

import businessLayer.*;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.util.*;

public class Serializator {

    public void serializeData(DeliveryService x){
        try{
            FileOutputStream userList = new FileOutputStream("userList.ser");
            FileOutputStream orderMap = new FileOutputStream("orderMap.ser");
            FileOutputStream productList = new FileOutputStream("productList.ser");
            FileOutputStream menuList = new FileOutputStream("menuList.ser");

            ObjectOutputStream users = new ObjectOutputStream(userList);
            users.writeObject(x.getUserList());
            ObjectOutputStream orders = new ObjectOutputStream(orderMap);
            orders.writeObject(x.getOrders());
            ObjectOutputStream products = new ObjectOutputStream(productList);
            products.writeObject(x.getProductsList());
            ObjectOutputStream menu = new ObjectOutputStream(menuList);
            menu.writeObject(x.getMenuList());


            userList.close();
            orderMap.close();
            productList.close();
            users.close();
            orders.close();
            products.close();
            menuList.close();
            menu.close();

        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    public void deserializeData(DeliveryService x){
        try{
            FileInputStream userList = new FileInputStream("userList.ser");
            FileInputStream orderMap = new FileInputStream("orderMap.ser");
            FileInputStream productList = new FileInputStream("productList.ser");
            FileInputStream menuList = new FileInputStream("menuList.ser");

            ObjectInputStream users = new ObjectInputStream(userList);
            x.setUserList((ArrayList<User>) users.readObject());
            ObjectInputStream orders = new ObjectInputStream(orderMap);
            x.setOrders((Map<Order, ArrayList<MenuItem>>) orders.readObject());
            ObjectInputStream products = new ObjectInputStream(productList);
            x.setProductsList((HashSet<MenuItem>) products.readObject());
            ObjectInputStream menu = new ObjectInputStream(menuList);
            x.setMenuList((ArrayList<MenuItem>) menu.readObject());

            userList.close();
            orderMap.close();
            productList.close();
            users.close();
            orders.close();
            products.close();
            menuList.close();
            menu.close();

        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
}
