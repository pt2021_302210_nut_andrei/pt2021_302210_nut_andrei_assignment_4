package dataLayer;

import businessLayer.MenuItem;
import businessLayer.Order;

import java.io.FileWriter;
import java.util.ArrayList;

public class BillGenerator {

    public BillGenerator(Order x, ArrayList<MenuItem> y) {
        int price = 0;
        try{
            String fileName = "bill" + x.getOrderID() + ".txt";
            FileWriter bill = new FileWriter(fileName);
            StringBuilder message = new StringBuilder();
            message.append("Order ");
            message.append(x.getOrderID());
            message.append("\nWith items:\n");
            for(MenuItem a : y){
                message.append(a.getTitle());
                message.append("\n");
                price += a.getPrice();
            }
            message.append("\nTotal:");
            message.append(price);
            message.append("\nDate:");
            message.append(x.getTime());
            bill.write(message.toString());
            bill.close();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
}
