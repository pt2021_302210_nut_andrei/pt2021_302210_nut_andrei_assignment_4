package dataLayer;

import businessLayer.Client;
import businessLayer.MenuItem;
import businessLayer.Order;

import java.io.FileWriter;
import java.util.List;

public class ReportGenerator {

    public void generateFirst(List<Order> x){
        try{
            FileWriter a = new FileWriter("FirstReport.txt");
            for(Order b : x){
                a.write(b.toString());
                a.write("\n");
            }
            a.close();
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    public void generateSecond(List<MenuItem> x){
        try{
            FileWriter a = new FileWriter("SecondReport.txt");
            for(MenuItem b : x){
                a.write(b.toString());
                a.write("\n");
            }
            a.close();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }

    public void generateThird(List<Client> x){
        try{
            FileWriter a = new FileWriter("ThirdReport.txt");
            for(Client b : x){
                a.write(b.toString());
                a.write("\n");
            }
            a.close();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }

    public void generateFourth(List<Order> x){
        try{
            FileWriter a = new FileWriter("FourthReport.txt");
            for(Order b : x){
                a.write(b.toString());
                a.write("\n");
            }
            a.close();
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
}
