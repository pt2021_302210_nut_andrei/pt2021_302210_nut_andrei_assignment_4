package presentationLayer;

import businessLayer.DeliveryService;
import dataLayer.Serializator;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import javax.swing.JPasswordField;

public class LoginGUI extends JFrame {

    private JPanel contentPane;
    private JPasswordField passwordField;
    private JTextField usernameField;
    private JButton loginButton;
    private JButton registerButton;
    private JLabel lblNewLabel;
    private JLabel lblPassword;
    private DeliveryService deliveryService;


    public LoginGUI(DeliveryService deliveryService) {
        setTitle("Login");
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        setBounds(100, 100, 500, 360);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        loginButton = new JButton("Login");
        loginButton.setBounds(191, 199, 89, 23);
        contentPane.add(loginButton);

        registerButton = new JButton("Register");
        registerButton.setBounds(191, 253, 89, 23);
        contentPane.add(registerButton);

        lblNewLabel = new JLabel("username:");
        lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
        lblNewLabel.setBounds(46, 83, 89, 34);
        contentPane.add(lblNewLabel);

        lblPassword = new JLabel("password:");
        lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 14));
        lblPassword.setBounds(46, 144, 89, 34);
        contentPane.add(lblPassword);

        passwordField = new JPasswordField();
        passwordField.setBounds(191, 153, 89, 25);
        contentPane.add(passwordField);

        usernameField = new JTextField();
        usernameField.setBounds(191, 92, 89, 25);
        contentPane.add(usernameField);
        usernameField.setColumns(10);

        this.addWindowListener(new java.awt.event.WindowAdapter(){
            @Override
            public void windowClosing(java.awt.event.WindowEvent e){
                new Serializator().serializeData(deliveryService);
                e.getWindow().dispose();
            }
        });
    }

    public String getUsername(){
        return this.usernameField.getText();
    }

    public String getPassword(){
        return String.valueOf(this.passwordField.getPassword());
    }

    public void addLoginActionListener(ActionListener actionListener){
        this.loginButton.addActionListener(actionListener);
    }

    public void addRegisterActionListener(ActionListener actionListener){
        this.registerButton.addActionListener(actionListener);
    }
}

