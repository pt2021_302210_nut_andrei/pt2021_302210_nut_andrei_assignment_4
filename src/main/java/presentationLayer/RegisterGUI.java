package presentationLayer;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;

public class RegisterGUI extends JFrame {

    private JPanel contentPane;
    private JTextField textField;
    private JPasswordField passwordField;
    private JPasswordField confirmPassField;
    private JLabel lblNewLabel;
    private JLabel lblPassword;
    private JLabel lblConfirmPassword;
    private JButton registerButton;
    private JLabel lblRole;
    private JComboBox comboBox;

    public RegisterGUI() {
        setTitle("Register");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 500, 400);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        lblNewLabel = new JLabel("username");
        lblNewLabel.setBounds(119, 59, 107, 28);
        contentPane.add(lblNewLabel);

        lblPassword = new JLabel("password");
        lblPassword.setBounds(119, 127, 101, 28);
        contentPane.add(lblPassword);

        textField = new JTextField();
        textField.setBounds(236, 63, 86, 20);
        contentPane.add(textField);
        textField.setColumns(10);

        passwordField = new JPasswordField();
        passwordField.setBounds(230, 131, 101, 24);
        contentPane.add(passwordField);

        lblConfirmPassword = new JLabel("confirm password");
        lblConfirmPassword.setBounds(119, 196, 101, 28);
        contentPane.add(lblConfirmPassword);

        confirmPassField = new JPasswordField();
        confirmPassField.setBounds(230, 200, 101, 24);
        contentPane.add(confirmPassField);

        registerButton = new JButton("Register");
        registerButton.setBounds(176, 308, 89, 23);
        contentPane.add(registerButton);

        lblRole = new JLabel("role");
        lblRole.setBounds(119, 265, 101, 28);
        contentPane.add(lblRole);

        String[] values = {"Employee" , "Customer"};
        comboBox = new JComboBox(values);
        comboBox.setBounds(236, 265, 101, 28);
        contentPane.add(comboBox);
    }

    public void addRegisterActionListener(ActionListener actionListener){
        this.registerButton.addActionListener(actionListener);
    }

    public String getSelectedItem(){
        return this.comboBox.getSelectedItem().toString();
    }

    public String getUsername(){
        return String.valueOf(textField.getText());
    }

    public String getFirstPassField(){
        return String.valueOf(passwordField.getPassword());
    }

    public String getSecondPassField(){
        return String.valueOf(confirmPassField.getPassword());
    }
}
