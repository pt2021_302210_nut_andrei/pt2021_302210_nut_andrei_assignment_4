package presentationLayer;


import businessLayer.MenuItem;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import static businessLayer.DeliveryService.productsList;

public class ProductsManagementGUI extends JFrame {

    private JPanel contentPane;
    private JTable table;
    private JButton btnRemoveItem;
    private JButton btnNewButton;
    private JScrollPane scrollPane;
    private JTextField titleField;
    private JTextField ratingField;
    private JTextField caloriesField;
    private JTextField proteinsField;
    private JTextField fatsField;
    private JTextField sodiumField;
    private JTextField priceField;
    private JButton updateBtn;
    private DefaultTableModel defaultModel;
    private JTextField nameField;


    public ProductsManagementGUI() {
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 800, 505);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        scrollPane = new JScrollPane();
        scrollPane.setBounds(10, 11, 601, 444);
        contentPane.add(scrollPane);

        Object[] columns = new Object[]{"Title","Rating","Calories","Protein","Fat","Sodium","Price"};
        defaultModel = new DefaultTableModel(columns,0);
        table = new JTable(defaultModel);
        scrollPane.setViewportView(table);

        btnNewButton = new JButton("Add Composite Item");
        btnNewButton.setBounds(621, 31, 158, 57);
        contentPane.add(btnNewButton);

        btnRemoveItem = new JButton("Remove item");
        btnRemoveItem.setBounds(621, 131, 158, 57);
        contentPane.add(btnRemoveItem);

        nameField = new JTextField();
        nameField.setBounds(621, 99, 153, 20);
        contentPane.add(nameField);
        nameField.setColumns(10);

        titleField = new JTextField();
        titleField.setBounds(693, 199, 86, 20);
        contentPane.add(titleField);
        titleField.setColumns(10);

        ratingField = new JTextField("-1");
        ratingField.setColumns(10);
        ratingField.setBounds(693, 225, 86, 20);
        contentPane.add(ratingField);

        caloriesField = new JTextField("-1");
        caloriesField.setColumns(10);
        caloriesField.setBounds(693, 256, 86, 20);
        contentPane.add(caloriesField);

        proteinsField = new JTextField("-1");
        proteinsField.setColumns(10);
        proteinsField.setBounds(693, 287, 86, 20);
        contentPane.add(proteinsField);

        fatsField = new JTextField("-1");
        fatsField.setColumns(10);
        fatsField.setBounds(693, 321, 86, 20);
        contentPane.add(fatsField);

        sodiumField = new JTextField("-1");
        sodiumField.setColumns(10);
        sodiumField.setBounds(693, 352, 86, 20);
        contentPane.add(sodiumField);

        priceField = new JTextField("-1");
        priceField.setColumns(10);
        priceField.setBounds(693, 386, 86, 20);
        contentPane.add(priceField);

        JLabel lblNewLabel = new JLabel("Title");
        lblNewLabel.setBounds(621, 199, 52, 20);
        contentPane.add(lblNewLabel);

        JLabel lblRating = new JLabel("Rating");
        lblRating.setBounds(621, 228, 52, 20);
        contentPane.add(lblRating);

        JLabel lblCalories = new JLabel("Calories");
        lblCalories.setBounds(621, 259, 52, 20);
        contentPane.add(lblCalories);

        JLabel lblProteins = new JLabel("Proteins");
        lblProteins.setBounds(621, 290, 52, 20);
        contentPane.add(lblProteins);

        JLabel lblFats = new JLabel("Fats");
        lblFats.setBounds(621, 324, 52, 20);
        contentPane.add(lblFats);

        JLabel lblSodium = new JLabel("Sodium");
        lblSodium.setBounds(621, 355, 52, 20);
        contentPane.add(lblSodium);

        JLabel lblPrice = new JLabel("Price");
        lblPrice.setBounds(621, 386, 52, 20);
        contentPane.add(lblPrice);

        updateBtn = new JButton("Update");
        updateBtn.setBounds(690, 432, 89, 23);
        contentPane.add(updateBtn);

    }

    public void addRemoveAction(ActionListener actionListener){
        this.btnRemoveItem.addActionListener(actionListener);
    }

    public void addNewProductAction(ActionListener actionListener){
        this.btnNewButton.addActionListener(actionListener);
    }

    public int[] getSelectedItems(){
        return this.table.getSelectedRows();
    }

    public void addRow(MenuItem x){
        defaultModel.addRow(new Object[]{x.getTitle(),x.getRating(),x.getCalories(),x.getProteins(),x.getFats(),x.getSodium(),x.getPrice()});
    }
    public void doChanges(Object name,Object rating,Object calories,Object protein, Object fat, Object sodium, Object price){
        if(!name.equals("")){
            defaultModel.setValueAt(name,getRowToDelete(),0);
        }
        if((float)rating != -1){
            defaultModel.setValueAt(rating,getRowToDelete(),1);
        }
        if((float)calories != -1){
            defaultModel.setValueAt(calories,getRowToDelete(),2);
        }
        if((float)protein != -1){
            defaultModel.setValueAt(protein,getRowToDelete(),3);
        }
        if((float)fat != -1){
            defaultModel.setValueAt(fat,getRowToDelete(),4);
        }
        if((float)sodium != -1){
            defaultModel.setValueAt(sodium,getRowToDelete(),5);
        }
        if((float)price != -1){
            defaultModel.setValueAt(price,getRowToDelete(),6);
        }
    }

    public void deleteRow(int index){
        defaultModel.removeRow(index);
    }

    public int getRowToDelete(){
        return this.table.getSelectedRow();
    }

    public String getNewProductName(){
        return this.nameField.getText();
    }

    public void addUpdateAction(ActionListener actionListener){
        this.updateBtn.addActionListener(actionListener);
    }

    public String getTitle(){
        return this.titleField.getText();
    }
    public float getRating(){
        return Float.parseFloat(this.ratingField.getText());
    }
    public float getCalories(){
        return Float.parseFloat(this.caloriesField.getText());
    }
    public float getProteins(){
        return Float.parseFloat(this.proteinsField.getText());
    }
    public float getFats(){
        return Float.parseFloat(this.fatsField.getText());
    }
    public float getSodium(){
        return Float.parseFloat(this.sodiumField.getText());
    }
    public float getPrice(){
        return Float.parseFloat(this.priceField.getText());
    }
}

