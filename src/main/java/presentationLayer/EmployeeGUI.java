package presentationLayer;

import businessLayer.Employee;
import businessLayer.Observer;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.Observable;

public class EmployeeGUI extends JFrame {

    private static JTextArea orderLabel;
    private JPanel contentPane;

    public EmployeeGUI(){
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 600, 430);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        orderLabel = new JTextArea();
        orderLabel.setBounds(10, 11, 564, 369);
        contentPane.add(orderLabel);
    }

   public static JTextArea getLabel(){
        return orderLabel;
   }
}
