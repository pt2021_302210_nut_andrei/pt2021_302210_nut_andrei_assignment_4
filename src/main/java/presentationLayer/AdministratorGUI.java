package presentationLayer;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;

public class AdministratorGUI extends JFrame {

    private JPanel contentPane;
    private JButton btnImportProducts;
    private JButton btnManageProducts;
    private JButton btnGenerateReports;
    private JButton btnImportData;

    public AdministratorGUI() {
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 555, 400);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        btnImportProducts = new JButton("Import Products");
        btnImportProducts.setBounds(10, 132, 151, 65);
        contentPane.add(btnImportProducts);

        btnManageProducts = new JButton("Manage Products");
        btnManageProducts.setBounds(194, 132, 151, 65);
        contentPane.add(btnManageProducts);

        btnGenerateReports = new JButton("Generate Reports");
        btnGenerateReports.setBounds(378, 132, 151, 65);
        contentPane.add(btnGenerateReports);

        btnImportData = new JButton("Import Data");
        btnImportData.setBounds(194, 239, 151, 65);
        contentPane.add(btnImportData);

    }

    public void addImportAction(ActionListener actionListener){
        this.btnImportProducts.addActionListener(actionListener);
    }

    public void addManageAction(ActionListener actionListener){
        this.btnManageProducts.addActionListener(actionListener);
    }

    public void addGenerateReports(ActionListener actionListener){
        this.btnGenerateReports.addActionListener(actionListener);
    }

    public void addImportData(ActionListener actionListener){
        this.btnImportData.addActionListener(actionListener);
    }

}

