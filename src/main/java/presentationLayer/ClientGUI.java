package presentationLayer;

import businessLayer.BaseProduct;
import businessLayer.MenuItem;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionListener;

import static businessLayer.DeliveryService.productsList;

public class ClientGUI extends JFrame {

    private JPanel contentPane;
    private JTextField caloriesField;
    private JTextField proteinsField;
    private JTextField fatsField;
    private JTextField sodiumField;
    private JTextField priceField;
    private JButton btnSearch;
    private JButton btnAddOrder;
    private JButton btnGetOrder;
    private JTextField nameField;
    private JTextField ratingField;
    private JLabel lblNewLabel;
    private JLabel lblRating;
    private JLabel lblCalories;
    private JLabel lblProteins;
    private JLabel lblFats;
    private JLabel lblSodium;
    private JLabel lblPrice;
    private JScrollPane scrollPane;
    private JTable table;
    private DefaultTableModel defaultModel;


    public ClientGUI() {
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 700, 476);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        btnSearch = new JButton("Search");
        btnSearch.setBounds(573, 68, 89, 23);
        contentPane.add(btnSearch);

        btnAddOrder = new JButton("Add to Order");
        btnAddOrder.setBounds(573, 369, 101, 23);
        contentPane.add(btnAddOrder);

        btnGetOrder = new JButton("Get Order");
        btnGetOrder.setBounds(573, 403, 89, 23);
        contentPane.add(btnGetOrder);

        nameField = new JTextField();
        nameField.setBounds(10, 27, 86, 20);
        contentPane.add(nameField);
        nameField.setColumns(10);

        ratingField = new JTextField("-1");
        ratingField.setColumns(10);
        ratingField.setBounds(106, 27, 86, 20);
        contentPane.add(ratingField);

        caloriesField = new JTextField("-1");
        caloriesField.setColumns(10);
        caloriesField.setBounds(202, 27, 86, 20);
        contentPane.add(caloriesField);

        proteinsField = new JTextField("-1");
        proteinsField.setColumns(10);
        proteinsField.setBounds(298, 27, 86, 20);
        contentPane.add(proteinsField);

        fatsField = new JTextField("-1");
        fatsField.setColumns(10);
        fatsField.setBounds(394, 27, 86, 20);
        contentPane.add(fatsField);

        sodiumField = new JTextField("-1");
        sodiumField.setColumns(10);
        sodiumField.setBounds(490, 27, 86, 20);
        contentPane.add(sodiumField);

        priceField = new JTextField("-1");
        priceField.setColumns(10);
        priceField.setBounds(588, 27, 86, 20);
        contentPane.add(priceField);

        lblNewLabel = new JLabel("Name");
        lblNewLabel.setBounds(10, 0, 86, 20);
        contentPane.add(lblNewLabel);

        lblRating = new JLabel("Rating");
        lblRating.setBounds(106, 0, 86, 20);
        contentPane.add(lblRating);

        lblCalories = new JLabel("Calories");
        lblCalories.setBounds(202, 3, 86, 20);
        contentPane.add(lblCalories);

        lblProteins = new JLabel("Proteins");
        lblProteins.setBounds(298, 3, 86, 20);
        contentPane.add(lblProteins);

        lblFats = new JLabel("Fats");
        lblFats.setBounds(394, 3, 86, 20);
        contentPane.add(lblFats);

        lblSodium = new JLabel("Sodium");
        lblSodium.setBounds(490, 3, 86, 20);
        contentPane.add(lblSodium);

        lblPrice = new JLabel("Price");
        lblPrice.setBounds(588, 3, 86, 20);
        contentPane.add(lblPrice);

        scrollPane = new JScrollPane();
        scrollPane.setBounds(20, 58, 543, 368);
        contentPane.add(scrollPane);

        Object[] columns = new Object[]{"Title","Rating","Calories","Protein","Fat","Sodium","Price"};
        defaultModel = new DefaultTableModel(columns,0);
        table = new JTable(defaultModel);
        scrollPane.setViewportView(table);

    }

    public void addRow(MenuItem x){
        defaultModel.addRow(new Object[]{x.getTitle(),x.getRating(),x.getCalories(),x.getProteins(),x.getFats(),x.getSodium(),x.getPrice()});
    }

    public void clearTable(){
        while(defaultModel.getRowCount() != 0){
            defaultModel.removeRow(0);
        }
    }

    public int getSelectedRow(){
        return table.getSelectedRow();
    }

    public void addGetOrderBtnListener(ActionListener actionListener){
        this.btnGetOrder.addActionListener(actionListener);
    }

    public void addAddOrderListener(ActionListener actionListener){
        this.btnAddOrder.addActionListener(actionListener);
    }

    public void addSearchBtnListener(ActionListener actionListener){
        this.btnSearch.addActionListener(actionListener);
    }

    public String getName(){
        return this.nameField.getText();
    }
    public float getRating(){
        return Float.parseFloat(this.ratingField.getText());
    }
    public float getCalories(){
        return Float.parseFloat(this.caloriesField.getText());
    }
    public float getProteins(){
        return Float.parseFloat(this.proteinsField.getText());
    }
    public float getFats(){
        return Float.parseFloat(this.fatsField.getText());
    }
    public float getSodium(){
        return Float.parseFloat(this.sodiumField.getText());
    }
    public float getPrice(){
        return Float.parseFloat(this.priceField.getText());
    }

}

