package presentationLayer;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class ReportGUI extends JFrame {

    private JPanel contentPane;
    private JButton firstBtn;
    private JButton secondBtn;
    private JButton thirdBtn;
    private JButton fourthBtn;
    private JTextField startField;
    private JTextField stopField;
    private JTextField secondField;
    private JTextField ordersField;
    private JTextField valueField;
    private JTextField dayField;
    private JLabel lblNewLabel;
    private JLabel lblEndTime;
    private JLabel lblNumberOfOrders;
    private JLabel lblValue;
    private JLabel lblDay;
    private JLabel lblOrderedMoreThan;


    public ReportGUI() {
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        firstBtn = new JButton("First report");
        firstBtn.setBounds(10, 11, 89, 23);
        contentPane.add(firstBtn);

        secondBtn = new JButton("Second report");
        secondBtn.setBounds(109, 11, 101, 23);
        contentPane.add(secondBtn);

        thirdBtn = new JButton("Third report");
        thirdBtn.setBounds(222, 11, 89, 23);
        contentPane.add(thirdBtn);

        fourthBtn = new JButton("Fourth Report");
        fourthBtn.setBounds(321, 11, 103, 23);
        contentPane.add(fourthBtn);

        startField = new JTextField();
        startField.setBounds(10, 76, 86, 20);
        contentPane.add(startField);
        startField.setColumns(10);

        stopField = new JTextField();
        stopField.setBounds(10, 107, 86, 20);
        contentPane.add(stopField);
        stopField.setColumns(10);

        secondField = new JTextField();
        secondField.setBounds(109, 76, 86, 20);
        contentPane.add(secondField);
        secondField.setColumns(10);

        ordersField = new JTextField();
        ordersField.setBounds(222, 76, 86, 20);
        contentPane.add(ordersField);
        ordersField.setColumns(10);

        valueField = new JTextField();
        valueField.setBounds(222, 107, 86, 20);
        contentPane.add(valueField);
        valueField.setColumns(10);

        dayField = new JTextField();
        dayField.setBounds(331, 55, 86, 20);
        contentPane.add(dayField);
        dayField.setColumns(10);

        lblNewLabel = new JLabel("Start time");
        lblNewLabel.setBounds(10, 51, 89, 14);
        contentPane.add(lblNewLabel);

        lblEndTime = new JLabel("End time");
        lblEndTime.setBounds(10, 135, 89, 14);
        contentPane.add(lblEndTime);

        lblNumberOfOrders = new JLabel("Number of orders");
        lblNumberOfOrders.setBounds(222, 45, 89, 14);
        contentPane.add(lblNumberOfOrders);

        lblValue = new JLabel("Value");
        lblValue.setBounds(222, 138, 89, 14);
        contentPane.add(lblValue);

        lblDay = new JLabel("Day");
        lblDay.setBounds(328, 86, 89, 14);
        contentPane.add(lblDay);

        lblOrderedMoreThan = new JLabel("Ordered more than");
        lblOrderedMoreThan.setBounds(109, 51, 101, 14);
        contentPane.add(lblOrderedMoreThan);
    }

    public void addFirstReport(ActionListener actionListener){
        this.firstBtn.addActionListener(actionListener);
    }
    public void addSecondReport(ActionListener actionListener){
        this.secondBtn.addActionListener(actionListener);
    }
    public void addThirdReport(ActionListener actionListener){
        this.thirdBtn.addActionListener(actionListener);
    }
    public void addFourthReport(ActionListener actionListener) {
        this.fourthBtn.addActionListener(actionListener);
    }

    public int getStartTime(){
        return Integer.parseInt(this.startField.getText());
    }
    public int getFinishTime(){
        return Integer.parseInt(this.stopField.getText());
    }
    public int getSecondFiled(){
        return Integer.parseInt(this.secondField.getText());
    }
    public int getOrdersField(){
        return Integer.parseInt(this.ordersField.getText());
    }
    public int getValue(){
        return Integer.parseInt(this.valueField.getText());
    }
    public String getDay(){
        return this.dayField.toString().toUpperCase();
    }

}
