import businessLayer.DeliveryService;
import controllers.LoginControl;
import presentationLayer.LoginGUI;

public class MainClass {

    public static void main(String[] args){

        DeliveryService x = new DeliveryService();
        x.addAdmin();
        new LoginControl(new LoginGUI(x),x);
    }
}
