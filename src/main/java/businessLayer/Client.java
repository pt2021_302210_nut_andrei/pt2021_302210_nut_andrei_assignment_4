package businessLayer;

import java.io.Serializable;

public class Client extends User implements Serializable {
    private int orders = 0;

    public Client(String username, String password) {
        super(username, password);
    }

    public void increment(){
        this.orders++;
    }
    public int getOrders(){
        return this.orders;
    }

    @Override
    public String toString(){
        return this.getUsername();
    }
}
