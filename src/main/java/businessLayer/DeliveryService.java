package businessLayer;

import dataLayer.BillGenerator;
import dataLayer.ReportGenerator;
import presentationLayer.ClientGUI;

import java.awt.*;
import java.io.*;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

public class DeliveryService extends Observable implements IDeliveryServiceProcessing, Serializable {

    public static Map<Order, ArrayList<MenuItem>> orders = new HashMap<>();
    public static List<User> userList = new ArrayList<>();
    public static Set<MenuItem> productsList = new HashSet<>();
    public static ArrayList<MenuItem> menuList = new ArrayList<MenuItem>();
    public boolean invariant = true;

    public void calculateOrdersPerClient(){
        for(User x : userList){
            if(x instanceof Client){
                for(Order y : orders.keySet()){
                    if(y.getClient().getUsername().equals(x.getUsername())){
                        ((Client) x).increment();
                    }
                }
            }
        }
    }

    /**
     * @pre userList is not null
     *
     * @post admin account is added
     */
    public void addAdmin(){
        userList.add(new Administrator("admin","admin"));
    }


    /**
     * @pre user list is initialized
     * @param user
     * @post user is added to the list
     */
    public static void addUser(User user){
        assert userList != null;
        userList.add(user);
    }

    public boolean wellFormed(){
        return true;
    }

    /**
     * @pre user list not null
     * @param username
     * @return
     */
    public static User getUser(String username){
        assert userList != null;

        for(User x : userList){
            if(x.getUsername().equals(username)){
                return x;
            }
        }
        return null;
    }

    public ArrayList<MenuItem> getMenuList(){
        return menuList;
    }

    public void setMenuList(ArrayList<MenuItem> x){
        menuList = x;
    }

    public void addMenuItem(MenuItem x){
        menuList.add(x);
    }

    /**
     * @pre userList not null
     * @param username
     * @return
     * @post user is in list or not
     */
    public static boolean userExists(String username) {
        assert userList != null;
        for (User x : userList) {
            if (x.getUsername().equals(username)) {
                return true;
            }
        }
        return false;
    }

    public List<User> getUserList(){
        return userList;
    }

    public void setUserList(ArrayList<User> x){
        userList = x;
    }

    public Set<MenuItem> getProductsList(){
        return productsList;
    }

    public void setProductsList(HashSet<MenuItem> x){
        productsList = x;
    }

    public Map<Order, ArrayList<MenuItem>> getOrders(){
        return orders;
    }

    public void setOrders(Map<Order,ArrayList<MenuItem>> x){
        orders = x;
    }

    /**
     * @pre menuList not null
     * @param index
     * @post product removed
     */

    @Override
    public void removeProduct(int index){
        assert menuList != null;
        menuList.remove(index);
    }

    /**
     * @pre orders must not be null
     * @param start the hour from which we start to compare
     * @param end the hour to which we stop to compare
     * @post goodOrders must contain something
     */
    @Override
    public void generateFirstReport(int start, int end) {
        assert orders != null;

        List<Order> goodOrders = orders.entrySet()
                .stream()
                .filter(e -> e.getKey().getTime().getHour() >= start && e.getKey().getTime().getHour() <= end)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
        new ReportGenerator().generateFirst(goodOrders);

        assert !goodOrders.isEmpty();
    }
    /**
     * @pre orders must not be null
     * @post goodOrders must contain something
     */
    @Override
    public void generateSecondReport(int number) {
        List<MenuItem> orderedProducts = menuList.stream()
                .filter(e -> e.getTimesOrdered() >= number)
                .collect(Collectors.toList());
        new ReportGenerator().generateSecond(orderedProducts);
    }
    /**
     * @pre orders must not be null
     * @post goodOrders must contain something
     */
    @Override
    public void generateThirdReport(int noOfTimes, float value) {
        List<Client> clients = orders.keySet()
                .stream()
                .filter(menuItems -> menuItems.getClient().getOrders() > noOfTimes)
                .filter(menuItems -> menuItems.getPrice() > value)
                .map(Order::getClient)
                .collect(Collectors.toList());
        new ReportGenerator().generateThird(clients);
    }
    /**
     * @pre orders must not be null
     * @post goodOrders must contain something
     */
    @Override
    public void generateFourthReport(String day) {
        Calendar calendar = Calendar.getInstance();
        List<Order> dayOrders = orders.entrySet()
                .stream()
                .filter(e -> e.getKey().getTime().getDayOfWeek().toString().equals(day))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
//        List<MenuItem> dayProducts = new ArrayList<>();

        new ReportGenerator().generateFourth(dayOrders);

    }

    /**
     * @pre index>=0
     * @pre name != null
     * @pre rating != -1
     * @pre calories != -1
     * @pre protein != -1
     * @pre fat != -1
     * @pre sodium != -1
     * @pre price != -1
     * @param index
     * @param name
     * @param rating
     * @param calories
     * @param protein
     * @param fat
     * @param sodium
     * @param price
     * @post product is modified
     */

    @Override
    public void updateProduct(int index,String name,float rating,float calories,float protein, float fat, float sodium, float price){
        assert index >= 0;
        assert name != null;
        assert rating != -1;
        assert calories != -1;
        assert protein != -1;
        assert fat != -1;
        assert sodium != -1;
        assert price != -1;

        if(!name.equals("")){
            menuList.get(index).setTitle(name);
        }
        if(rating != -1){
            menuList.get(index).setRating(rating);
        }
        if(calories != -1){
            menuList.get(index).setCalories(calories);
        }
        if(protein != -1){
            menuList.get(index).setProteins(protein);
        }
        if(fat != -1){
            menuList.get(index).setFats(fat);
        }
        if(sodium != -1){
            menuList.get(index).setSodium(sodium);
        }
        if(price != -1){
            menuList.get(index).setPrice(price);
        }
    }

    /**
     * @pre Order x and ArrayList<> y not null
     * @param x
     * @param y
     * @post new order added
     */

    @Override
    public void createNewOrder(Order x, ArrayList<MenuItem> y){
        assert x != null && y != null;
        orders.put(x,y);
        y.forEach(MenuItem::increment);
        new BillGenerator(x,y);
        for(User a : userList){
            if(a instanceof Employee){
                a.update();
            }
        }
        calculateOrdersPerClient();
    }

    /**
     * @pre menuList not null
     * @param name
     * @param rating
     * @param calories
     * @param protein
     * @param fat
     * @param sodium
     * @param price
     * @return
     * @post filtered stream of data
     */
    @Override
    public List<MenuItem> searchItem(String name, float rating, float calories, float protein, float fat, float sodium, float price) {
        assert menuList != null;
        return menuList.stream()
                .filter(e -> e.getTitle().contains(name)
                        || e.getRating() == rating
                        || e.getCalories() == calories
                        || e.getProteins() == protein
                        || e.getFats() == fat
                        || e.getSodium() == sodium
                        || e.getPrice() == price)
                .collect(Collectors.toList());
    }
}
