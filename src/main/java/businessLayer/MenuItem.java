package businessLayer;

import java.io.Serializable;

public abstract class MenuItem implements Serializable {

    private int timesOrdered = 0;
    private String title;
    private float rating;
    private float calories;
    private float proteins;
    private float fats;
    private float sodium;
    private float price;

    public MenuItem(String title, float rating, float calories, float proteins, float fats, float sodium, float price) {
        this.title = title;
        this.rating = rating;
        this.calories = calories;
        this.proteins = proteins;
        this.fats = fats;
        this.sodium = sodium;
        this.price = price;
    }

    public String getTitle(){
        return this.title;
    }

    public float getRating(){
        return this.rating;
    }

    public float getCalories(){
        return this.calories;
    }

    public float getProteins(){
        return this.proteins;
    }

    public float getFats(){
        return this.fats;
    }

    public float getSodium(){
        return this.sodium;
    }

    public float getPrice(){
        return this.price;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public void setRating(float rating){
        this.rating = rating;
    }

    public void setCalories(float calories){
        this.calories = calories;
    }

    public void setProteins(float proteins){
        this.proteins = proteins;
    }

    public void setFats(float fats){
        this.fats = fats;
    }

    public void setSodium(float sodium){
        this.sodium = sodium;
    }

    public void setPrice(float price){
        this.price = price;
    }

    public void increment(){
        this.timesOrdered++;
    }

    public int getTimesOrdered(){
        return this.timesOrdered;
    }

    @Override
    public String toString(){
        return this.title + " " + timesOrdered;
    }

    public abstract float computePrice();
}
