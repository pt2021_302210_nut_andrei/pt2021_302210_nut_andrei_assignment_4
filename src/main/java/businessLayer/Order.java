package businessLayer;

import java.io.Serializable;
import java.time.LocalDateTime;

public class Order implements Serializable {

    private int orderID;
    private LocalDateTime dateTime;
    private Client client;
    private float price;

    public Order(int orderID,Client client,float price){
        this.orderID = orderID;
        this.price = price;
        this.client = client;
        dateTime = LocalDateTime.now();
    }

    public int getOrderID(){
        return this.orderID;
    }

    public LocalDateTime getTime(){
        return this.dateTime;
    }

    public Client getClient(){
        return this.client;
    }

    public float getPrice(){
        return this.price;
    }

    @Override
    public String toString(){
        return "Order " + orderID + " Time: " + dateTime;
    }
}
