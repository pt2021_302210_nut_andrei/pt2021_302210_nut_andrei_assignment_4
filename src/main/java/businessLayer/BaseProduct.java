package businessLayer;

import java.io.Serializable;

public class BaseProduct extends MenuItem implements Serializable {

    public BaseProduct(String title, float rating, float calories, float proteins, float fats, float sodium, float price){
        super(title,rating,calories,proteins,fats,sodium,price);
    }

    @Override
    public float computePrice() {
        return this.getPrice();
    }
}
