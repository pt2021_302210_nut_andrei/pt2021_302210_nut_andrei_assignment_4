package businessLayer;

import java.io.Serializable;

public class User extends Observer implements Serializable {

    private String username;
    private String password;

    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

    public String getUsername(){
        return this.username;
    }

    public String getPassword(){
        return this.password;
    }

    @Override
    public void update() {

    }
}
