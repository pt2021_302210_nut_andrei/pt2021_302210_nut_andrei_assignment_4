package businessLayer;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static businessLayer.DeliveryService.productsList;

public interface IDeliveryServiceProcessing {

    static void importProducts(){
        String fileName = "products.csv";
        File file = new File(fileName);
        try {
            Scanner inputStream = new Scanner(file);
//            inputStream.useDelimiter(",");
            inputStream.nextLine();
            while(inputStream.hasNextLine()){
                //System.out.println(inputStream.next());
                String line = inputStream.nextLine();
                String[] delimitedItems = line.split(",");
                String title = delimitedItems[0];
                float rating = Float.parseFloat(delimitedItems[1]);
                float calories = Float.parseFloat(delimitedItems[2]);
                float protein = Float.parseFloat(delimitedItems[3]);
                float fat = Float.parseFloat(delimitedItems[4]);
                float sodium = Float.parseFloat(delimitedItems[5]);
                float price = Float.parseFloat(delimitedItems[6]);
                productsList.add(new BaseProduct(title,rating,calories,protein,fat,sodium,price));
            }
            inputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    /**
     * @pre index>=0
     * @pre name != null
     * @pre rating != -1
     * @pre calories != -1
     * @pre protein != -1
     * @pre fat != -1
     * @pre sodium != -1
     * @pre price != -1
     * @param index
     * @param name
     * @param rating
     * @param calories
     * @param protein
     * @param fat
     * @param sodium
     * @param price
     * @post product is modified
     */
    void updateProduct(int index,String name,float rating,float calories,float protein, float fat, float sodium, float price);
    /**
     * @pre menuList not null
     * @param index
     * @post product removed
     */
    void removeProduct(int index);
    /**
     * @pre orders must not be null
     * @post goodOrders must contain something
     */
    void generateFirstReport(int start, int end);
    /**
     * @pre orders must not be null
     * @post goodOrders must contain something
     */
    void generateSecondReport(int number);
    /**
     * @pre orders must not be null
     * @post goodOrders must contain something
     */
    void generateThirdReport(int noOfTimes, float value);
    /**
     * @pre orders must not be null
     * @post goodOrders must contain something
     */
    void generateFourthReport(String day);
    /**
     * @pre Order x and ArrayList<> y not null
     * @param x
     * @param y
     * @post new order added
     */
    void createNewOrder(Order x, ArrayList<MenuItem> y);
    /**
     * @pre menuList not null
     * @param name
     * @param rating
     * @param calories
     * @param protein
     * @param fat
     * @param sodium
     * @param price
     * @return
     * @post filtered stream of data
     */
    List<MenuItem> searchItem(String name, float rating, float calories, float protein, float fat, float sodium, float price);
}
