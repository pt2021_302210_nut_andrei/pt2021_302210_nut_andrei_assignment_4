package businessLayer;

import presentationLayer.EmployeeGUI;

import java.io.Serializable;

public class Employee extends User implements Serializable {

    public Employee(String username, String password) {
        super(username, password);
    }

    @Override
    public void update(){
        EmployeeGUI.getLabel().setText(EmployeeGUI.getLabel().getText() + "\nAn order has been added!");
    }
}
